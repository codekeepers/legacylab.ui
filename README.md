# LegacyLab UI

## Developing

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

## Deploy
```bash
scp -r build/* root@95.217.180.178:/var/www/ll.ralfwirdemann.de
```